let random;
export default random = {
  directionsMultiplier: {
    0: -1,
    1: 1
  },
  getRandomNum: function(max) {
    return Math.floor(Math.random() * Math.floor(max));
  },
  jump: function(amp) {
    return (
      this.directionsMultiplier[this.getRandomNum(2)] * this.getRandomNum(amp)
    );
  }
};
