export default class UI {
  constructor(game) {
    this.game = game;
    this.startButtton = document.getElementById("start");
    this.messageContainer = document.getElementById("messageWidth");
    this.messages = { start: "Start!", success: "Success!" };
    this.messageContainer.innerText = this.messages.start;
    this.messageContainer.classList.add("visible");

    this.startButtton.onclick = () => {
      this.startGame();
    };

    this.rocketsInfoContainer = document.getElementById("rockets-info");
    this.displaySideInfo();
  }

  startGame() {
    this.game.startGame();
    this.messageContainer.innerText = this.messages.success;
    this.messageContainer.classList.remove("visible");
  }

  restartGame() {
    this.game.restart();
  }

  displaySideInfo() {
    let templateConcat = "";

    this.game.rockets.forEach((rocket, i) => {
      let descTemplate = `<div class="rocket-desc">
      <h2 class="rocket-title">${rocket.name}</h2>
      <p class="fuel-left">Fuel(Tons):${Math.round(rocket.fuel, 2)}</p>
      <p class="country">Country:${rocket.country}</p>
      <p class="company">Company:${rocket.company}</p>
      <p class="description"></p>
    </div>`;
      templateConcat += descTemplate;

      if (this.game.rockets.length - 1 === i) {
        this.rocketsInfoContainer.innerHTML = templateConcat;
      }
    });

    if (!this.game.started) {
      this.messageContainer.classList.add("visible");
    }
  }
}
