export default class Background {
  constructor(ctx, gameWidth, gameHeight) {
    this.gameWidth = gameWidth;
    this.gameHeight = gameHeight;

    this.ctx = ctx;
    this.imageElement = document.getElementById("background");

    this.imageElement.width = this.imageElement.naturalWidth;
    this.imageElement.height = this.imageElement.naturalHeight;

    this.infiniteImageLoaded = false;
    this.offsetBottom = 0;
    this.scrolling = true;

    this.createInfiniteImg();
  }

  createInfiniteImg() {
    let tempCanvas = document.createElement("canvas");
    let tempCtx = tempCanvas.getContext("2d");

    tempCanvas.width = this.imageElement.width;
    tempCanvas.height = this.imageElement.height * 2;
    tempCtx.drawImage(this.imageElement, 0, 0);
    tempCtx.save();

    tempCtx.translate(0, tempCanvas.height);
    tempCtx.scale(1, -1);
    tempCtx.drawImage(this.imageElement, 0, 0);
    tempCtx.restore();

    this.infiniteImage = document.createElement("img");
    this.infiniteImage.src = tempCanvas.toDataURL();

    this.infiniteImageHeight = this.imageElement.height * 2;

    this.infiniteImage.onload = () => {
      this.loadImage();
    };
  }

  startScrolling() {
    this.scrolling = true;
  }

  stopScrolling() {
    this.scrolling = false;
  }

  loadImage() {
    this.infiniteImageLoaded = true;
  }

  pan() {
    if (!this.scrolling) {
      return;
    }

    this.offsetBottom += 1;
    if (this.offsetBottom > this.infiniteImageHeight) {
      this.offsetBottom = 0;
    }

    this.ctx.drawImage(this.infiniteImage, 0, this.offsetBottom);
    this.ctx.drawImage(
      this.infiniteImage,
      0,
      -(this.infiniteImageHeight - this.offsetBottom)
    );
  }
}
