export default class Game {
  constructor(gameWidth, gameHeight, rockets, background) {
    this.gameWidth = gameWidth;
    this.gameHeight = gameHeight;
    this.rockets = rockets;
    this.background = background;

    this.initRocketsMeasures();
    this.setRocketsCanvasPositions();
    this.rocketsMoving = true;

    this.started = false;
  }

  startGame() {
    this.started = true;
    this.background.startScrolling();
    this.rockets.forEach(rocket => {
      rocket.launched = true;
      rocket.setInterval();
    });
  }

  restartGame() {
    this.started = false;
    this.background.stopScrolling();
    this.initRocketsMeasures();

    this.setRocketsCanvasPositions();
    this.rockets.forEach(rocket => {
      rocket.restartRocket();
    });
  }

  update() {
    if (!this.started) {
      this.background.stopScrolling();
      return;
    }

    if (!this.rockets.find(rocket => !rocket.arrived)) {
      this.started = false;
      this.restartGame();
      return;
    }

    this.rockets.forEach(rocket => {
      let rocketMoving = true;

      if (
        rocket.distancePassed > this.gameHeight / 2 &&
        rocket.totalDistance - rocket.distancePassed > this.gameHeight / 2
      ) {
        rocketMoving = false;
      }

      rocket.update(rocketMoving);
    });
  }

  draw(ctx) {
    if (this.background.infiniteImageLoaded) {
      this.background.pan();
    }

    this.rockets
      .filter(rocket => !rocket.arrived)
      .forEach((rocket, i) => {
        rocket.draw(ctx);
      });
  }

  setRocketsCanvasPositions() {
    this.rockets.forEach((rocket, i) => {
      rocket.setStartThrustPosX(this.calculateThrustDistX(rocket, i));
      rocket.setStartThrustPosY(this.calculateThrustDistY(rocket, i));
      rocket.setStartBodyPosX(this.calculateBodyDistX(rocket, i));
      rocket.setStartBodyPosY(this.calculateBodyDistY(rocket, i));
    });
  }

  initRocketsMeasures() {
    this.rockets.forEach(rocket => {
      rocket.initializeMeasures();
    });
  }

  calculateBodyDistX(rocket, rocketOrder) {
    return (
      (this.gameWidth / this.rockets.length) * rocketOrder +
      this.gameWidth / this.rockets.length / 3
    );
  }

  calculateBodyDistY(rocket, rocketOrder) {
    return this.gameHeight - rocket.startHeight - rocket.startThrustHeight;
  }

  calculateThrustDistX(rocket, rocketOrder) {
    return (
      (this.gameWidth / this.rockets.length) * rocketOrder +
      this.gameWidth / this.rockets.length / 3 +
      (rocket.startWidth - rocket.startThrustWidth) / 2
    );
  }

  calculateThrustDistY(rocket, rocketOrder) {
    return this.gameHeight - rocket.startThrustHeight;
  }
}
