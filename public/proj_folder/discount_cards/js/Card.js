const Card = function (id, name, email, city, category, accumulation, discount, dateArr) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.city = city;
    this.category = category;
    this.accumulation = accumulation;
    this.discount = discount.padStart(2, '0');
    this.date = new Date();
    this.date.setDate(dateArr[2]);
    this.date.setMonth(dateArr[1] - 1);
    this.date.setYear(dateArr[0]);

    let day = this.date.getDate();
    let month = this.date.getMonth() + 1;
    let year = this.date
        .getFullYear()
        .toString()
        .substr(-2);

    this.code = this.category
        + this.accumulation
        + this.discount
        + day.toString().padStart(2, '0')
        + month.toString().padStart(2, '0')
        + year;

}


export { Card };