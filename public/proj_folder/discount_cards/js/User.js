const User = function(name, email, cards){
    let id = window.localStorage.getItem('id');

    this.id = id++;
    this.name = name;
    this.email = email;
    this.cards = new Array();

    window.localStorage.setItem('id', id);
}

User.prototype.hasCategoryCard= function(category) {
    return this.getCardsArray()
    .filter(card => card.category == category);
}

User.prototype.getCardsArray = function () {
    return JSON.parse(this.cards);
}

User.prototype.addCardId = function(id){
    this.cards.push(id);
}


export {User};