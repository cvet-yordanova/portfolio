import { Card } from "./Card.js";
import { html } from "./constants.js";
import { User } from "./User.js";

const CardsDb = function(usersDb) {
  window.localStorage.setItem("cardsDb", JSON.stringify(new Array()));
  this.cards = window.localStorage.getItem("cardsDb");

  this.usersDb = usersDb;
};

CardsDb.prototype.sort = function(sortCriteria) {
  localStorage.setItem(
    "cardsDb",
    JSON.stringify(this.getCardsArray().sort(sortCriteria))
  );

  this.cards = window.localStorage.getItem("cardsDb");
};

CardsDb.prototype.getCardById = function(id) {
  return this.getCardsArray().filter(c => c.id == id)[0];
};

CardsDb.prototype.getCardsArray = function() {
  return JSON.parse(this.cards);
};

CardsDb.prototype.deleteCardById = function(id) {
  let idx = this.getCardsArray().findIndex(entry => entry.id == id);

  let tempArr = this.getCardsArray();
  tempArr.splice(idx, 1);

  localStorage.setItem("cardsDb", JSON.stringify(tempArr));
  this.cards = localStorage.getItem("cardsDb");
};

CardsDb.prototype.createCard = function(
  name,
  city,
  email,
  category,
  accumulation,
  discount,
  date
) {
  let id = window.localStorage.getItem("id");

  let newCard = new Card(
    id++,
    name,
    email,
    city,
    category,
    accumulation,
    discount,
    date
  );

  if (!this.usersDb.existsUser(email)) {
    let newUser = new User(name, email);
    newUser.addCardId(newCard.id);
    this.usersDb.addUser(newUser);
  } else {
    //now it returns just obj without methods
    let tempArr = this.usersDb.getUsersArray();
    let tempUser = this.usersDb.getUserByEmail(email);
    let alreadyHasCard = false;

    tempUser.cards.forEach(cardId => {
      if (this.getCardById(cardId).category == category) {
        alreadyHasCard = true;
      }
    });

    if (alreadyHasCard) {
      alert("user already has card from this category");
      return;
    }

    tempUser.cards.push(newCard.id);
    let idx = tempArr.findIndex(entry => entry.email == email);

    tempArr.splice(idx, 1, tempUser);

    window.localStorage.setItem("usersDb", JSON.stringify(tempArr));
    this.usersDb.users = window.localStorage.getItem("usersDb");
    window.localStorage.setItem("id", id);

    this.usersDb.cards = window.localStorage.getItem("usersDb");
  }

  localStorage.setItem(
    "cardsDb",
    JSON.stringify(this.getCardsArray().concat(newCard))
  );
  this.cards = localStorage.getItem("cardsDb");
};

CardsDb.prototype.editCardById = function(id) {
  // get existing card
  let card = this.getCardsArray().filter(c => c.id == id)[0];
  //get values from form to update card
  card.name = html.formName.value;
  card.city = html.formCity.value;
  card.category = html.formCategory.value;
  card.accumulation = html.formAccumulation.checked ? "1" : "0";
  card.discount = html.formPercentage.value.padStart(2, "0");

  //parse date
  let dateArr = html.formExpirationDate.value.split("-");
  let expirationDate = new Date(html.formExpirationDate.value);
  // card.expirationDate = expirationDate;
  card.date = expirationDate;

  let day = new Date(expirationDate).getDate();
  let month = new Date(expirationDate).getMonth();
  let year = new Date(expirationDate)
    .getFullYear()
    .toString()
    .substr(-2);

  card.code =
    card.category +
    card.accumulation +
    card.discount +
    day.toString().padStart(2, "0") +
    month.toString().padStart(2, "0") +
    year;

  //get idx of the existing array and replace it
  let idx = this.getCardsArray().findIndex(entry => entry.id == id);
  let tempArr = this.getCardsArray();
  tempArr.splice(idx, 1, card);

  localStorage.setItem("cardsDb", JSON.stringify(tempArr));

  this.cards = localStorage.getItem("cardsDb");
};

CardsDb.prototype.existsUserEmail = function(email) {
  this.getCardsArray.filter(card => card.email === email);
};

CardsDb.prototype.renewExpired = function(id) {
  let card = this.getCardById(id);
  let idx = this.getCardsArray().findIndex(entry => entry.id == id);

  let newDate = new Date();
  newDate.setFullYear(newDate.getFullYear() + 1);

  card.date = newDate;

  let day = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate
    .getFullYear()
    .toString()
    .substr(-2);

  card.code =
    card.category +
    card.accumulation +
    card.discount +
    day.toString().padStart(2, "0") +
    month.toString().padStart(2, "0") +
    year;

  let tempArr = this.getCardsArray();
  tempArr.splice(idx, 1, card);

  localStorage.setItem("cardsDb", JSON.stringify(tempArr));
  this.cards = localStorage.getItem("cardsDb");
};

CardsDb.prototype.createCards = function createCards() {
  let arrCards = [];
  let id = window.localStorage.getItem("id");
  arrCards.push(
    new Card(
      id++,
      "Mariq Petrova",
      "email",
      "Pavlikeni",
      "1",
      "1",
      "30",
      Date.now()
    )
  );
  arrCards.push(
    new Card(id++, "Gosho Goshov", "email", "Sofiq", "2", "1", "30", Date.now())
  );
  arrCards.push(
    new Card(
      id++,
      "Stefan Petrov",
      "email",
      "Drqnovo",
      "3",
      "1",
      "30",
      Date.now()
    )
  );
  arrCards.push(
    new Card(
      id++,
      "Ginka Petrova",
      "email",
      "Varna",
      "4",
      "1",
      "30",
      Date.now()
    )
  );

  window.localStorage.setItem("id", id);
  localStorage.setItem("cardsDb", JSON.stringify(arrCards));
  this.cards = JSON.stringify(arrCards);

  return arrCards;
};

export { CardsDb };
