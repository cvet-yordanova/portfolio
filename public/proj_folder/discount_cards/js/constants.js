export const html =
{
    cardTableBody: document.getElementById('bodyCards'),

    cardCode: document.getElementById('code'),
    editCardBtn: document.getElementById('submitEdit'),
    showAddCard: document.getElementById('showAddCard'),
    addCard: document.getElementById('addCard'),
    addCardBtn: document.getElementById('submitAdd'),
    cardForm: document.getElementById('cardForm'),
    overlay: document.getElementById('overlay'),

    nameAsc: document.getElementById('nameAsc'),
    nameDesc: document.getElementById('nameDesc'),
    cityAsc: document.getElementById('cityAsc'),
    cityDesc: document.getElementById('cityDesc'),
    codeAsc: document.getElementById('codeAsc'),
    codeDesc: document.getElementById('codeDesc'),

    category: document.getElementById('category'),
    percentage: document.getElementById('percentage'),
    expirationDate: document.getElementById('expirationDate'),
    accumulation: document.getElementById('accumulation'),

    formTitle: document.getElementById('formTitle'),
    form: document.getElementById('form'),
    formName: document.getElementById('formName'),
    formEmail: document.getElementById('formEmail'),
    formCity: document.getElementById('formCity'),
    formCategory: document.getElementById('formCategory'),
    formAccumulation: document.getElementById('formAccumulation'),
    formPercentage: document.getElementById('formPercentage'),
    formExpirationDate: document.getElementById('formExpirationDate'),

    searchBox1: document.getElementById('seachbox-1'),
    searchBox2: document.getElementById('seachbox-2'),
    searchBox2Toggle: document.getElementById('seachbox-2-toggle'),
    searchBox3: document.getElementById('seachbox-3'),
    searchBox3Toggle: document.getElementById('seachbox-3-toggle'),

    closeForm: document.getElementById('closeForm'),

    searchName: document.getElementById('searchName'),
    searchCity: document.getElementById('searchCity'),
    searchCode: document.getElementById('seachCode'),

    changeTables: document.getElementById('changeTables'),
    tableCards: document.getElementById('tableCards'),
    tableUsers: document.getElementById('tableUsers'),
    showUsers: document.getElementById('showUsers'),
    showCards: document.getElementById('showCards'),
    usersBodyTable: document.getElementById('bodyUsers')
}
