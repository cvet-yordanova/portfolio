const UsersDb = function() {
  window.localStorage.setItem("usersDb", JSON.stringify(new Array()));
  this.users = window.localStorage.getItem("usersDb");
  this.cards = new Array();
};

UsersDb.prototype.getUsersArray = function() {
  return JSON.parse(this.users);
};

UsersDb.prototype.existsUser = function(email) {
  return this.getUsersArray().filter(user => user.email === email).length > 0;
};

UsersDb.prototype.getUserByEmail = function(email) {
  return this.getUsersArray().filter(user => user.email === email)[0];
};

UsersDb.prototype.addUser = function(user) {
  localStorage.setItem(
    "usersDb",
    JSON.stringify(this.getUsersArray().concat(user))
  );
  this.users = localStorage.getItem("usersDb");
};

export { UsersDb };
