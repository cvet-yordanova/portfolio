import { html } from './constants.js';

export function validateForm() {

    let regexPatterns = {
        string: /[a-zA-z]+/,
        category:/[1234]{1}/,
        date: /^((19|20)\d{2})-((0|1)\d{1})-((0|1|2)\d{1})/,
        discount: /5|10|20|30/
    }
    
    let allValid = true;
    let errInputs = [];

    removeErrorStyles();

    if (!regexPatterns.string.test(html.formName.value)) {
        allValid = false;
        errInputs.push(html.formName);
    }

    if (!regexPatterns.string.test(html.formCity.value)) {
        allValid = false;
        errInputs.push(html.formCity);
    }

    if (!regexPatterns.category.test(html.formCategory.value)) {
        allValid = false;
        errInputs.push(html.formCategory);
    }

    if (!regexPatterns.discount.test(html.formPercentage.value)) {

        allValid = false;
        errInputs.push(html.formPercentage);
    }

    if (!regexPatterns.date.test(html.formExpirationDate.value)) {
     
        allValid = false;
        errInputs.push(html.formExpirationDate);
    }

    errInputs.forEach(function (input) {
        addErrorStyles(input);

    });

    if (allValid) {
        removeErrorStyles();
    }

    return allValid;

}

function addErrorStyles(element) {
    element.classList.add('err');
}

function removeErrorStyles() {
    let inputs = document.getElementsByClassName('err');

    while (inputs[0]) {
        inputs[0].classList.remove('err')
    }

}