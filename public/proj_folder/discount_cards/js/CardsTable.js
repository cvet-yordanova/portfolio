import { html } from "./constants.js";
import { rowTemplate } from "./templates.js";
import { rowUserTemplate } from "./templates.js";

export const CardsTable = {
  table: document.getElementById("cardsTable"),
  showCardForm: function(id) {
    html.editCardBtn.setAttribute("data-id", id);
    html.overlay.style.display = "flex";
    html.cardForm.style.display = "block";
  },
  fillFormData: function(card) {
    html.formName.value = card.name;
    html.formCity.value = card.city;
    html.formCategory.value = card.category;
    html.formEmail.value = card.email;

    html.formAccumulation.setAttribute(
      "checked",
      card.accumulation == "1" ? true : false
    );

    html.formPercentage.value = card.discount;

    let cardExpirationDate = new Date(card.date);

    html.formExpirationDate.value = cardExpirationDate
      .toISOString()
      .substr(0, 10);
  },
  updateRow: function(card) {
    let rowData = Array.from(document.querySelectorAll("tr")).filter(a => {
      return a.dataset.id == card.id;
    })[0].children;

    rowData[0].innerHTML = card.name;
    rowData[1].innerHTML = card.city;
    rowData[2].innerHTML = card.code;
  },
  populateTableRows: function populateTableCards(arrSorted) {
    let table = html.cardTableBody;
    table.innerHTML = "";

    for (let i = 0; i < arrSorted.length; i++) {
      let currentCard = arrSorted[i];

      table.innerHTML += rowTemplate.format([
        currentCard.id,
        currentCard.name,
        currentCard.city,
        currentCard.code
      ]);
    }
  },
  populateUsersTable: function populateUsersTable(arrayUsers, cardsDb) {
    let table = html.usersBodyTable;
    let arrCards = [];

    table.innerHTML = "";

    for (let i = 0; i < arrayUsers.length; i++) {
      let currentUser = arrayUsers[i];
      arrCards = [];
      currentUser.cards.forEach(cardId => {
        arrCards.push(cardsDb.getCardById(cardId));
      });

      let htmlCards = "";
      arrCards.forEach(card => {
        let expiredStyle = new Date(card.date) < Date.now() ? "expired" : "";
        htmlCards += `<span class="card-span ${expiredStyle}" data-id=${card.id}> ${card.code}</span>`;
      });

      table.innerHTML += rowUserTemplate.format([
        currentUser.id,
        currentUser.name,
        currentUser.email,
        htmlCards
      ]);
    }
  },
  deleteRow: function(cardId) {
    if (confirm("Are you sure you want to delete this")) {
      let arr = Array.from(document.querySelectorAll("tr")).filter(a => {
        return a.dataset.id === cardId;
      })[0];

      if (arr) {
        arr.remove();
      }
    }
  }
};
