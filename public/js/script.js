(function() {
  var Navigation = window.Navigation || {};
  var Projects = window.Projects || {};

  Projects = (function() {
    function Projects() {
      let _ = this;

      _.options = {
        element: document.getElementById("translate-img"),
        easing: 0.5, //0.1
        dragSpeed: 4, //1
        duration: 400 //750
      };

      _.translateImgWrap = document.getElementById("translate-img");
      _.translateDescWrap = document.getElementById("translate-desc");
      _.projectImagesArr = document.getElementsByClassName("project-wrap");
      _.projectDescriptions = document.getElementsByClassName("project-desc");
      _.dotIndicatorsWrap = document.getElementById("dotsWrap");
      _.dotIndicators = document.getElementsByClassName("ind-dot");
      _.translateImgWrapWidth = _.translateImgWrap.getBoundingClientRect().width;

      _.isMouseDown = false;
      _.isTranslating = false;
      _.currentPosition = 0;
      _.startPosition = 0;
      _.endPosition = 0;
      _.translation = 0;
      _.animationFrame = null;

      this.setupSlider();
      this.initializeDots();
    }

    return Projects;
  })();

  Projects.prototype.initializeDots = function() {
    _ = this;
    [].slice.call(_.projectImagesArr).forEach((projImage, idx) => {
      let newDot = document.createElement("li");
      newDot.setAttribute(
        "data-proj-order",
        projImage.getAttribute("data-proj-order")
      );
      newDot.classList.add("ind-dot");
      if (idx === 0) {
        newDot.classList.add("active");
      }
      _.dotIndicatorsWrap.append(newDot);
    });

    _.projDotsAttachEventClick();
  };

  Projects.prototype.projDotsAttachEventClick = function() {
    _ = this;

    [].slice.call(_.dotIndicators).forEach(dot => {
      dot.addEventListener("click", function() {
        let order = Number(dot.getAttribute("data-proj-order"));
        let translateValue = _.translateImgWrapWidth * (order - 1) * -1;
        _.updateDotActiveClass(order);
        _.translateSliderClick(translateValue);
      });
    });
  };

  Projects.prototype.updateDotActiveClass = function(projOrder) {
    _ = this;

    [].slice.call(_.dotIndicators).forEach(dot => {
      dot.classList.remove("active");

      if (dot.getAttribute("data-proj-order") == projOrder) {
        dot.classList.add("active");
      }
    });
  };

  Projects.prototype.lerp = function(value1, value2, amount) {
    amount = amount < 0 ? 0 : amount;
    amount = amount > 1 ? 1 : amount;
    return (1 - amount) * value1 + amount * value2;
  };

  Projects.prototype.getMousePosition = function(e) {
    var mousePosition;
    if (e.targetTouches) {
      if (e.targetTouches[0]) {
        mousePosition = [
          e.targetTouches[0].clientX,
          e.targetTouches[0].clientY
        ];
      } else if (e.changedTouches[0]) {
        // handling touch end event
        mousePosition = [
          e.changedTouches[0].clientX,
          e.changedTouches[0].clientY
        ];
      } else {
        // fallback
        mousePosition = [e.clientX, e.clientY];
      }
    } else {
      mousePosition = [e.clientX, e.clientY];
    }

    return mousePosition;
  };

  Projects.prototype.setBoundaries = function() {
    let _ = this;
    _.boundaries = {
      max: -1 * _.translateImgWrapWidth * (_.projectImagesArr.length - 1),
      min: 0,
      sliderSize: _.options.element.clientWidth,
      referentSize: window.innerWidth
    };

    _.direction = 0;
  };

  Projects.prototype.translateSliderClick = function(translation) {
    var direction = "translateX";
    // apply translation
    _.options.element.style.transform = direction + "(" + translation + "px)";
    _.translation = translation;
    _.currentPosition = translation;

    _.translateDesc(translation);
  };

  Projects.prototype.translateDesc = function(translation) {
    let projOrder = Math.floor(
      Math.abs(translation) / _.translateImgWrapWidth + 1
    );
    let leftPxTranslated = Math.abs(translation) % _.translateImgWrapWidth;

    let percentLeft = (leftPxTranslated / _.translateImgWrapWidth) * 100;

    let targetDesc = document.querySelector(
      "div[data-proj-order='" + projOrder + "']"
    );

    let calculatedLeftPx = (targetDesc.offsetHeight * percentLeft) / 100;

    let descTranslationDist = (targetDesc.offsetTop + calculatedLeftPx) * -1;
    var direction = "translateY";

    _.updateDotActiveClass(projOrder);
    _.translateDescWrap.style.transform =
      direction + "(" + descTranslationDist + "px)";
  };

  Projects.prototype.translateSlider = function(translation) {
    let _ = this;
    translation = Math.floor(translation * 100) / 100;

    // should we translate it horizontally or vertically?
    // var direction = _.direction === 0 ? "translateX" : "translateY";
    var direction = "translateX";
    // apply translation
    _.options.element.style.transform = direction + "(" + translation + "px)";

    // if the slider translation is different than the translation to apply
    // that means the slider is still translating
    if (_.translation !== translation) {
      // hook function to execute while we are translating
      // _.onTranslation();
      _.translateDesc(translation);
    } else if (_.isTranslating && !_.isMouseDown) {
      // if those conditions are met, that means the slider is no longer translating
      _.isTranslating = false;

      // hook function to execute after translation has ended
      // _.onTranslationEnded();
    }
    // finally set our translation
    _.translation = translation;
  };

  Projects.prototype.animate = function() {
    let _ = this;
    // interpolate values
    var translation = _.lerp(
      _.translation,
      _.currentPosition,
      _.options.easing
    );
    // apply our translation
    _.translateSlider(translation);

    _.animationFrame = requestAnimationFrame(_.animate.bind(_));
  };

  Projects.prototype.onMouseDown = function(e) {
    // start dragging
    let _ = this;
    _.isMouseDown = true;

    // apply specific styles
    _.options.element.classList.add("dragged");

    // get our touch/mouse start position
    var mousePosition = _.getMousePosition(e);
    // use our slider direction to determine if we need X or Y value
    _.startPosition = mousePosition[_.direction];

    // drag start hook
    // _.onDragStarted(mousePosition);
  };

  Projects.prototype.onMouseMove = function(e) {
    let _ = this;
    // if we are not dragging, we don't do nothing
    if (!_.isMouseDown) return;

    // get our touch/mouse position
    var mousePosition = _.getMousePosition(e);

    // get our current position
    _.currentPosition =
      _.endPosition +
      (mousePosition[_.direction] - _.startPosition) * _.options.dragSpeed;

    // if we're not hitting the boundaries
    if (
      _.currentPosition > _.boundaries.min &&
      _.currentPosition < _.boundaries.max
    ) {
      // if we moved that means we have started translating the slider
      _.isTranslating = true;
    } else {
      // clamp our current position with boundaries
      _.currentPosition = Math.min(_.currentPosition, _.boundaries.min);
      _.currentPosition = Math.max(_.currentPosition, _.boundaries.max);
    }

    // drag hook
    // _.onDrag(mousePosition);
  };

  Projects.prototype.onMouseUp = function(e) {
    // we have finished dragging
    let _ = this;
    _.isMouseDown = false;

    // remove specific styles
    _.options.element.classList.remove("dragged");

    // update our end position
    _.endPosition = _.currentPosition;

    // send our mouse/touch position to our hook
    var mousePosition = _.getMousePosition(e);

    // drag ended hook
    // _.onDragEnded(mousePosition);
  };

  Projects.prototype.onResize = function(e) {
    // get our old translation ratio
    let _ = this;
    var ratio = _.translation / _.boundaries.sliderSize;

    // reset boundaries and properties bound to window size
    _.setBoundaries();

    // reset all translations
    _.options.element.style.transform = "tanslate3d(0, 0, 0)";

    // calculate our new translation based on the old translation ratio
    var newTranslation = ratio * _.boundaries.sliderSize;
    // clamp translation to the new boundaries
    newTranslation = Math.min(newTranslation, _.boundaries.min);
    newTranslation = Math.max(newTranslation, _.boundaries.max);

    // apply our new translation
    _.translateSlider(newTranslation);

    // reset current and end positions
    _.currentPosition = newTranslation;
    _.endPosition = newTranslation;

    // call our resize hook
    // _.onSliderResized();
  };

  Projects.prototype.setupSlider = function() {
    let _ = this;
    _.setBoundaries();

    // event listeners

    // mouse events
    window.addEventListener("mousemove", _.onMouseMove.bind(_), {
      passive: true
    });
    window.addEventListener("mousedown", _.onMouseDown.bind(_));
    window.addEventListener("mouseup", _.onMouseUp.bind(_));

    // touch events
    window.addEventListener("touchmove", _.onMouseMove.bind(_), {
      passive: true
    });
    window.addEventListener("touchstart", _.onMouseDown.bind(_), {
      passive: true
    });
    window.addEventListener("touchend", _.onMouseUp.bind(_));

    // resize event
    window.addEventListener("resize", _.onResize.bind(_));

    // launch our request animation frame loop
    _.animate();
  };

  Navigation = (function() {
    function Navigation() {
      var _ = this;
      _.screenWrapper = document.getElementById("boxes");
      _.screens = document.getElementsByClassName("screen");
      _.screenHeight = _.screens[0].getBoundingClientRect().height;
      _.navItems = document.getElementsByClassName("screen-nav-item");
      _.navToggle = document.getElementById("navtoggle");
      _.leftSideNav = document.getElementById("sidenav");
      _.minTranslateValue = (_.screens.length - 1) * _.screenHeight * -1;
      _.maxTranslateValue = 0;
      _.currentTranslation = 0;
      _.xDown = null;
      _.offset = 40;
      _.screenArray = [
        { name: "home", order: "1", translateY: 0 },
        { name: "skills", order: "2", translateY: _.screenHeight * -1 },
        { name: "work", order: "3", translateY: _.screenHeight * 2 * -1 },
        {
          name: "education_1",
          order: "4",
          translateY: _.screenHeight * 3 * -1
        },
        {
          name: "education_2",
          order: "5",
          translateY: _.screenHeight * 4 * -1
        },
        { name: "contact", order: "6", translateY: _.screenHeight * 5 * -1 }
      ];
      _.events = [
        "mousewheel",
        "wheel",
        "DOMMouseScroll",
        "onmousewheel",
        "touchstart",
        "touchend"
      ];
    }

    return Navigation;
  })();

  Navigation.prototype.getAttr = function() {
    return el.getAttribute(attrName);
  };

  Navigation.prototype.translateSlide = function(translateValue) {
    let _ = this;
    changeActiveBoxes();
    _.screenWrapper.style.transform =
      "translate3d(0px," + translateValue + "px" + ",0px)";
    _.changeActiveNav(translateValue);
  };

  Navigation.prototype.changeActiveNav = function(translateValue) {
    let _ = this;
    let order = _.screenArray.find(s => s.translateY === translateValue).order;

    [].slice.call(_.navItems).forEach(navItem => {
      if (
        Array.from(navItem.getAttribute("data-screen-order")).includes(order)
      ) {
        navItem.classList.add("active");
      } else {
        navItem.classList.remove("active");
      }
    });
  };

  Navigation.prototype.changeSlide = function(direction) {
    let _ = this;
    let translateValue = _.currentTranslation;

    switch (direction) {
      case "top":
        if (translateValue + _.screenHeight > _.maxTranslateValue) {
          translateValue = _.minTranslateValue;
        } else {
          translateValue = translateValue + _.screenHeight;
        }
        _.currentTranslation = translateValue;
        break;

      case "bottom":
        if (translateValue + _.screenHeight * -1 < _.minTranslateValue) {
          translateValue = _.maxTranslateValue;
        } else {
          translateValue = translateValue + _.screenHeight * -1;
        }
        _.currentTranslation = translateValue;
        break;
      default:
        return;
    }

    _.translateSlide(translateValue);
  };

  Navigation.prototype.attachEvents = function() {
    let _ = this;

    _.navToggle.addEventListener("click", function() {
      _.leftSideNav.classList.toggle("active");
    });

    [].slice.call(_.navItems).forEach(function(navItem) {
      navItem.addEventListener("click", function() {
        let screenOrder = Array.from(
          navItem.getAttribute("data-screen-order")
        )[0];
        let translateValue = _.screenArray.find(s => s.order == screenOrder)
          .translateY;
        _.translateSlide(translateValue);
      });
    });

    [].slice.call(_.events).forEach(function(event) {
      [].slice.call(_.screens).forEach(function(screen) {
        switch (event) {
          case "touchstart":
            screen.addEventListener("touchstart", function(evt) {
              // function getTouches(evt) {
              //   return evt.touches || evt.originalEvent.touches;
              // }
              _.xDown = evt.touches[0].clientX;
              _.yDown = evt.touches[0].clientY;
            });
            break;

          case "touchend":
            screen.addEventListener("touchend", function(evt) {
              if (!_.xDown || !_.yDown) {
                return;
              }

              const { clientX: xUp, clientY: yUp } = evt.changedTouches[0];
              const yDiff = _.yDown - yUp;

              // if (Math.max(xDiffAbs, yDiffAbs) < offset) {
              if (Math.abs(yDiff) < _.offset) {
                return;
              }

              if (yDiff > 0) {
                direction = "bottom";
              } else if (yDiff < 0) {
                direction = "top";
              }

              _.changeSlide(direction);
            });

            break;

          default:
            screen.addEventListener(event, function(e) {
              let direction;

              if (e.deltaY > 0) {
                direction = "bottom";
              } else if (e.deltaY < 0) {
                direction = "top";
              }

              _.changeSlide(direction);

              e.preventDefault();
              // e.cancelBubble = false;
              // e.returnValue = false;
              return false;
            });
            break;
        }
      });
    });
  };

  let projects = new Projects();
  let nav = new Navigation();
  nav.attachEvents();
})();

const squareContainer = document.getElementById("boxes-decoration");
const squareWidth = squareContainer.getBoundingClientRect().width;
//5 is number of screens
const squareHeight = squareContainer.getBoundingClientRect().height;

//this should be according to screen width
const boxesWidth = Math.round(squareWidth / 7);
const boxesYCount = squareHeight / boxesWidth;
const totalBoxes = 7 * boxesYCount;

let topDistancePX = 0;
let leftDistancePX = 100;

for (let i = 1; i <= totalBoxes; i++) {
  let newBoxNode = document.createElement("div");
  newBoxNode.classList.add("el-box");
  newBoxNode.classList.add("hidden");
  newBoxNode.style.width = boxesWidth + "px";
  newBoxNode.style.height = boxesWidth + "px";
  newBoxNode.style.left = leftDistancePX + "px";
  newBoxNode.style.top = topDistancePX + "px";
  leftDistancePX += boxesWidth;
  if (i % 7 == 0) {
    topDistancePX += boxesWidth;
    leftDistancePX = 0;
  }

  squareContainer.append(newBoxNode);
}

populateBoxes();

function populateBoxes() {
  for (let i = 0; i < 7; i++) {
    showRandomBox();
  }
}

function hideAllSquares() {
  let allSquares = document.getElementsByClassName("el-box");
  [].slice.call(allSquares).forEach(function(square, i) {
    square.classList.add("hidden");
  });
}

function showRandomBox() {
  let allSquares = document.getElementsByClassName("el-box");
  let randomIdxFromTheSquares =
    Math.floor(Math.random() * allSquares.length) + 0;
  if (allSquares[randomIdxFromTheSquares].classList.contains("hidden")) {
    allSquares[randomIdxFromTheSquares].classList.remove("hidden");
  } else {
    showRandomBox();
  }
}

function changeActiveBoxes() {
  hideAllSquares();
  populateBoxes();
}
